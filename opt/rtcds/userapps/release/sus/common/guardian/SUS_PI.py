import numpy as np
import time
from timeout_utils import call_with_timeout

from guardian import GuardState
import cdsutils as cdu
import piparams


################################################
# STATES
#################################################
# request upon initialization
request = 'IDLE'
# nominal operational state
nominal = 'PI_DAMPING'

## TODO: Make it so we don't go to highbias for pi24 until we've tried all
## angles at least once (ex. takes 80s to go through all angles so try using
## 90s timer or something before upping the bias)

## TODO: Make it so when all modes on the arm are damped, we turn off the
## damping for that arm

PIs = piparams.PIs
all_chans = []
for pi in PIs:
    all_chans.append(f"SUS-PI_PROC_COMPUTE_MODE{pi['mode']}_RMSMON")

def gen_CHANGE_BIAS(bias):
    '''Generates states that adjust the bias on ETMY'''
    class CHANGE_BIAS(GuardState):
        request = False
        def main(self):
            
            self.bias = bias
            if self.bias == 'highbias':
                # we are changing ETMY bias to -9
                ezca['SUS-ETMY_L3_LOCK_BIAS_TRAMP'] = 30
                ezca['SUS-ETMY_L3_LOCK_BIAS_OFFSET'] = -9
            else:
                # If going to low bias, go back to our nominal bias value
                # Change ramp time back to 120 so we can go back into Observing
                ezca['SUS-ETMY_L3_LOCK_BIAS_TRAMP'] = 120
                ezca['SUS-ETMY_L3_LOCK_BIAS_OFFSET'] = -4.9
        
        def run(self):
            
            return True
            
    return CHANGE_BIAS


def gen_PI_DAMPING(bias):
    '''Generates two PI damping states for regular and extreme PI damping'''
    class DAMPING(GuardState):
        def main(self):
            
            self.bias = bias
            
            # ENGAGE PLL : turn on PLL integrators
            for pi in PIs:
                ezca.get_LIGOFilter(f"SUS-PI_PROC_COMPUTE_MODE{pi['mode']}_PLL_FREQ_FILT2").switch_on('FM3')
            
            self.phase_steps = 45
            self.direction = -1
            self.timer['checker'] = 10
            self.timer['ignore_omc_whitening'] = 60
            # Wait 5 seconds between each time we grab last 5s average
            self.timer['do_nothing_5s'] = 5
            
        def run(self):
            
            if ezca['GRD-ISC_LOCK_STATE_N'] < 200:
                log('ifo unlocked? going down')
                return 'IFO_DOWN'
            
            if ezca['GRD-ISC_LOCK_STATE_N'] > 580 and ezca['GRD-ISC_LOCK_STATE_N'] < 600:
                # In the first minute of OMC WHITENING, ignore PI damping
                if not self.timer['ignore_omc_whitening']:
                    return False
            
            # Grab current value
            if self.timer['do_nothing_5s']:
                all_pi_avg = call_with_timeout(
                    cdu.avg,
                    -5,
                    all_chans
                )
                for i in range(len(PIs)):
                    PIs[i]['currentrms'] = all_pi_avg[i]
                
                # Wait 5 seconds between each time we grab last 5s average
                self.timer['do_nothing_5s'] = 5
                
                for pi in PIs:
                    
                    if pi['upbias']:
                        if not self.timer['checker'] and self.bias == 'highbias':
                            return False
                    # If current value above threshold, turn on damping
                    if pi['currentrms'] > pi['thresh'] and self.timer['checker']:
                        # Turn on ESD + damp filter for PI
                        ezca[f"SUS-{pi['arm']}_PI_ESD_DRIVER_PI_DAMP_SWITCH"] = 1
                        ezca[f"SUS-PI_PROC_COMPUTE_MODE{pi['mode']}_DAMP_GAIN"] = pi['gain']
                        if ezca[f"SUS-PI_PROC_COMPUTE_MODE{pi['mode']}_PLL_LOCK_ST"] < 0.5:
                            notify(f"mode{pi['mode']} PLL not locking, check integrator")
                            ezca[f"SUS-PI_PROC_COMPUTE_MODE{pi['mode']}_PLL_FREQ_FILT2_RSET"] = 2
                        
                        pi['currentrms'] = call_with_timeout(
                            cdu.avg,
                            -5,
                            f"SUS-PI_PROC_COMPUTE_MODE{pi['mode']}_RMSMON"
                        )
                        if pi['currentrms']:
                            if pi['biasthresh'] is not None:
                                if pi['currentrms'] > pi['biasthresh'] and self.bias == 'lowbias':
                                    log(f"PI{pi['mode']} RMS is too high at "
                                        f"{np.round(pi['currentrms'],2)}. "
                                        f"{pi['arm']} ESD driver might be "
                                        "saturating. Going to up the bias."
                                    )
                                    pi['upbias'] = True
                                    return 'INCREASE_BIAS'
                            if pi['currentrms'] > pi['oldrms']:
                                notify(f"Scanning mode {pi['mode']} phase to damp")
                                new_phase = (ezca[f"SUS-PI_PROC_COMPUTE_MODE{pi['mode']}_PLL_PHASE"]+self.phase_steps*self.direction)%360
                                ezca[f"SUS-PI_PROC_COMPUTE_MODE{pi['mode']}_PLL_PHASE"]=new_phase
                                log(f"Wrong way? old rms={np.round(pi['oldrms'],2)}, new rms={np.round(pi['currentrms'],2)}")
                            else:
                                log(f"Going the right way, old rms={np.round(pi['oldrms'],2)}, new rms={np.round(pi['currentrms'],2)}")
                            self.timer['checker'] = 10
                            pi['oldrms'] = pi['currentrms']
                            if pi['biasthresh'] is not None:
                                if self.bias == 'highbias':
                                    return False
                        else:
                            notify('No data obtained from nds')
                    elif pi['currentrms'] < pi['thresh'] and self.bias == 'lowbias':
                        # Turn off damping for that PI
                        ezca[f"SUS-PI_PROC_COMPUTE_MODE{pi['mode']}_DAMP_GAIN"] = 0
                    elif pi['currentrms'] < pi['thresh'] and self.bias == 'highbias':
                        if pi['biasthresh'] is not None:
                            log(f"PI{pi['mode']} is back to normal. Lowering the "
                                f"bias on {pi['arm']}")
                            # Returning True now will either keep us in PI_DAMPING
                            # or take us to lower the bias
                            pi['upbias'] = False  # Reset need for upbias
                            return True
                    if PIs[-1] == pi:  # If last pi in list is good, return True
                        return True
            elif self.bias == 'lowbias':
                return True
    return DAMPING


class IFO_DOWN(GuardState):
    """
    Disable tracking and damping when the IFO unlocks. Turn off I/ETM bias.

    """
    index = 2
    
    def main(self):
        '''
        # Turn ETMX fast DCPD output gains to 0
        for mode in range(17,25):
            ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_GAIN'] = 0
        # Turn ETMY fast DCPD output gains to 0
        for mode in range(25,33):
            ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_GAIN'] = 0
        '''
        # Reset PI parameter values
        log('Resetting parameter values that were changed')
        for pi in PIs:
            pi['oldrms'] = pi['thresh']
            pi['currentrms'] = None
            pi['upbias'] = False
        
        # Turn off master output switch
        ezca['SUS-ETMX_PI_ESD_DRIVER_PI_DAMP_SWITCH'] = 0
        ezca['SUS-ETMY_PI_ESD_DRIVER_PI_DAMP_SWITCH'] = 0
        
        # turn off PLL integrators
        for pi in PIs:
            pll_base_name = f"SUS-PI_PROC_COMPUTE_MODE{pi['mode']}_PLL_"
            ezca.LIGOFilter(pll_base_name + 'FREQ_FILT1').turn_off('FM3')
            ezca.LIGOFilter(pll_base_name + 'FREQ_FILT2').turn_off('FM3')
            ezca[f"SUS-PI_PROC_COMPUTE_MODE{pi['mode']}_DAMP_GAIN"] = 0
            time.sleep(0.5)
            
    def run(self):
        if ezca['SUS-PI_PROC_COMPUTE_MODE24_RMSMON'] > 2 or ezca['SUS-PI_PROC_COMPUTE_MODE31_RMSMON'] > 3:
            if ezca['GRD-ISC_LOCK_STATE_N'] > 575:
                notify('going to PI_DAMPING')
                return 'PI_DAMPING'
        return True


class IDLE(GuardState):
    index = 1
    #goto=True
    def run(self):
        return True


class INIT(GuardState):
    index = 0
    request = True
    def main(self):
        log('Initializing Parametric Instability Guardian:')
        return True
        
        ''' pre-O4, TCS tuning excites modes on ETMY, settings guardians for ETMs only.
        # Turn ITMX output gains to 0
        for mode in range(1,9):
            ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_GAIN'] = 0 
        # Turn ITMY output gains to 0
        for mode in range(9,17):
            ezca[f'SUS-PI_PROC_COMPUTE_MODE{mode}_DAMP_GAIN'] = 0 
        # Ramp down the ITM bias
        ezca['SUS-ITMX_L3_LOCK_BIAS_GAIN'] = 0
        ezca['SUS-ITMY_L3_LOCK_BIAS_GAIN'] = 0
        # Turn off master output switch
        ezca['SUS-ITMX_PI_ESD_DRIVER_PI_DAMP_SWITCH'] = 0
        ezca['SUS-ITMY_PI_ESD_DRIVER_PI_DAMP_SWITCH'] = 0
        '''
        ''' was in 'IFO_DOWN' for loop
            if False:
                # extensive reset
                ezca[pll_base_name + 'FREQ_FILT1_GAIN'] = 3.0
                ezca[pll_base_name + 'SIGNAL_GAIN'] = 0.1
                ezca.LIGOFilter(pll_base_name + 'FREQ_COUNT').only_on('IN', 'OUT', 'DECIMATION', 'FM1', 'FM3')
                ezca.LIGOFilter(pll_base_name + 'AMP_FILT').only_on('IN', 'OUT', 'DECIMATION', 'FM1', 'FM2')
                ezca.LIGOFilter(pll_base_name + 'LOCK').only_on('IN', 'OUT', 'DECIMATION', 'FM1', 'FM2')
                ezca.LIGOFilter(pll_base_name + 'THETA').only_on('IN', 'OUT', 'DECIMATION', 'FM1', 'FM2')
                ezca.LIGOFilter(pll_base_name + 'I').only_on('IN', 'OUT', 'DECIMATION', 'FM1')
                ezca.LIGOFilter(pll_base_name + 'Q').only_on('IN', 'OUT', 'DECIMATION', 'FM1')
                ezca.LIGOFilter(pll_base_name + 'FREQ_COUNT').only_on('IN', 'OUT', 'DECIMATION', 'FM1', 'FM3')
                ezca.LIGOFilter(pll_base_name + 'FREQ_FILT1').only_on('IN', 'OUT', 'DECIMATION')
                ezca.LIGOFilter(pll_base_name + 'FREQ_FILT2').only_on('IN', 'OUT', 'DECIMATION')

class ITM_BIAS(GuardState):
    index = 5
    request = False
    """
    Turn on ITM bias

    """
    def main(self):
        # Ramp on (10 sec) the ITM bias
        ezca['SUS-ITMX_L3_LOCK_BIAS_GAIN'] = 1 # Added 11Aug2016.
        ezca['SUS-ITMY_L3_LOCK_BIAS_GAIN'] = 1
        self.timer['pause']=10
    def run(self):
        if self.timer:
            return True
'''

PI_DAMPING          = gen_PI_DAMPING(bias='lowbias')
XTREME_PI_DAMPING   = gen_PI_DAMPING(bias='highbias')
INCREASE_BIAS       = gen_CHANGE_BIAS(bias='highbias')
DECREASE_BIAS       = gen_CHANGE_BIAS(bias='lowbias')

PI_DAMPING.index = 10
XTREME_PI_DAMPING.index = 20
INCREASE_BIAS.index = 25
DECREASE_BIAS.index = 15


edges = [
    ('INIT', 'IFO_DOWN'),
    ('IFO_DOWN', 'PI_DAMPING'),
    ('PI_DAMPING', 'IDLE'),
    ('IDLE', 'PI_DAMPING'),
    ('PI_DAMPING', 'INCREASE_BIAS'),
    ('INCREASE_BIAS', 'XTREME_PI_DAMPING'),
    ('XTREME_PI_DAMPING', 'DECREASE_BIAS'),
    ('DECREASE_BIAS', 'PI_DAMPING'),
    ]
