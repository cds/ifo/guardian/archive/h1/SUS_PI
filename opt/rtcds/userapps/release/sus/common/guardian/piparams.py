
## CONFIG FILE FOR PI DAMPING GUARDIAN


############################
# PLL SETUP
#   PLLs should all work with the same filters and gains for I, Q, AMP, LOCK, THETA, etc.
#     (e.g., all gains are 1 except PLL_FREQ_FILT1_GAIN, which the guardian controls)
#     NOTE: PLL loop UGF is 1Hz * Gain
#
#   the input signal should be scaled using PLL_SIGNAL_GAIN such that
#     the noise give an AMP_FILT output somewhat less than 0.1
#
#   the PLL_SET_FREQ must match the mechanical mode frequency to within ~1Hz
#
#   the PLL_PHASE value should be tuned to set the feedback phase (maximize damping for a given gain)
#
#   and the damping gain value should be set to damp quickly, but not too quickly or
#     the PLL may unlock.  Saturation should also be avoided (e.g., gain < 30000).
############################


## PI dictionaries
pi24 = {            # PI24 - ETMY 10.431 kHz
    'mode': 24,
    'thresh': 6,
    'arm': 'ETMY',
    'gain': 10000,
    'oldrms': 6,
    'currentrms': None,
    'biasthresh': 40,
    'upbias': False
}
pi31 = {            # PI31 - ETMY 10.428 kHz
    'mode': 31,
    'thresh': 3,
    'arm': 'ETMY',
    'gain': 1000,
    'oldrms': 3,
    'currentrms': None,
    'biasthresh': None,
    'upbias': False
}
pi28 = {            # PI28 - ETMX 80.295 kHz
    'mode': 28,
    'thresh': 1,
    'arm': 'ETMX',
    'gain': 5000,
    'oldrms': 1,
    'currentrms': None,
    'biasthresh': None,
    'upbias': False
}
pi29 = {            # PI29 - ETMX 80.3 kHz
    'mode': 29,
    'thresh': 1,
    'arm': 'ETMX',
    'gain': 5000,
    'oldrms': 1,
    'currentrms': None,
    'biasthresh': None,
    'upbias': False
}
# Put them into a list - PI24 is last since it'll cause the most return False's
PIs = [pi31, pi28, pi29, pi24]
